namespace GameBoy

open GameBoy.APU
open GameBoy.CPU
open GameBoy.PPU
open GameBoy.Platform
open GameBoy.Platform.Util


type State =
  { apu: Apu
    cpu: CpuState
    pad: Pad
    ppu: Ppu
    tma: Tma

    bios: Memory
    hram: Memory
    oam: Memory
    vram: Memory
    wave: Memory
    wram: Memory

    mutable bootRomEnabled: bool }

module State =

  open System.IO
  open System.Reflection


  let private readAllBytes path =
    let assembly = Assembly.GetEntryAssembly () in
    let location = assembly.Location in
    let folder = Path.GetDirectoryName location in
    let absolutePath = Path.Combine (folder, path) in

    File.ReadAllBytes absolutePath


  let init biosFileName =
    { apu = Apu.init ()
      cpu = Cpu.init ()
      pad = Pad.init ()
      ppu = Ppu.init ()
      tma = Tma.init ()

      bios = Memory.rom (readAllBytes biosFileName)
      hram = Memory.ram (BinaryUnit.byte 128)
      oam  = Memory.ram (BinaryUnit.byte 256)
      vram = Memory.ram (BinaryUnit.kib 8)
      wave = Memory.ram (BinaryUnit.byte 16)
      wram = Memory.ram (BinaryUnit.kib 8)

      bootRomEnabled = true }
