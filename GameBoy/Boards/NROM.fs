module GameBoy.Boards.NROM

open GameBoy


let read cartridge = Reader (fun address ->
  match address with
  | n when n >= 0x0000us && n <= 0x7fffus -> cartridge |> Cartridge.readRom (int address)
  | n when n >= 0xa000us && n <= 0xbfffus -> cartridge |> Cartridge.readRam (int address)
  | _ ->
    0xffuy
)


let write cartridge = Writer (fun address data ->
  if address >= 0xa000us && address <= 0xbfffus then
    cartridge
      |> Cartridge.writeRam (int address) data
)
