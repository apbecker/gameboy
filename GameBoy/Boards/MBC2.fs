namespace GameBoy.Boards

open GameBoy.Platform

type MBC2 =
  { ram: Memory
    mutable romPage: int }

module MBC2 =

  open GameBoy
  open GameBoy.Platform.Util


  let init () =
    { ram = Memory.ram (BinaryUnit.byte 512)
      romPage = 1 }


  let read0000 cartridge mbc2 = Reader (fun address ->
    cartridge
      |> Cartridge.readRom ((int address) &&& 0x3fff)
  )


  let read4000 cartridge mbc2 = Reader (fun address ->
    cartridge
      |> Cartridge.readRom (((int address) &&& 0x3fff) ||| (mbc2.romPage <<< 14))
  )


  let readA000 mbc2 = Reader (fun address ->
    mbc2.ram
      |> Memory.get (int address)
  )


  let write0000 mbc2 = Writer (fun address data ->
    if (address &&& 0x100us) = 0us then
      ()
    )


  let write2000 mbc2 = Writer (fun address data ->
    if (address &&& 0x100us) <> 0us then
      mbc2.romPage <- ((int data) &&& 0x1f)

      if mbc2.romPage = 0 then
        mbc2.romPage <- 1
    )


  let writeA000 mbc2 = Writer (fun address data ->
    mbc2.ram
      |> Memory.put (int address) (data &&& 0x0fuy))


  let read cartridge mbc2 = Reader (fun address ->
    match address with
    | n when n >= 0x0000us && n <= 0x3fffus -> Reader.run (read0000 cartridge mbc2) address
    | n when n >= 0x4000us && n <= 0x7fffus -> Reader.run (read4000 cartridge mbc2) address
    | n when n >= 0xa000us && n <= 0xbfffus -> Reader.run (readA000 mbc2) address
    | _ ->
      0xffuy
  )


  let write mbc2 = Writer (fun address data ->
    match address with
    | n when n >= 0x0000us && n <= 0x1fffus -> Writer.run (write0000 mbc2) address data
    | n when n >= 0x2000us && n <= 0x3fffus -> Writer.run (write2000 mbc2) address data
    | n when n >= 0x4000us && n <= 0x5fffus -> ()
    | n when n >= 0x6000us && n <= 0x7fffus -> ()
    | n when n >= 0xa000us && n <= 0xbfffus -> Writer.run (writeA000 mbc2) address data
    | _ ->
      ()
  )
