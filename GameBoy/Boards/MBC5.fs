namespace GameBoy.Boards

type MBC5 =
  { mutable ramEnabled: bool
    mutable ramPage: int
    mutable romPage: int }

module MBC5 =

  open GameBoy


  let init () =
    { ramEnabled = false
      ramPage = 0
      romPage = 0 }


  let private ramAddress address mbc5 =
    ((int address) &&& 0x1fff) ||| mbc5.ramPage


  let private read0000 cartridge mbc5 = Reader (fun address ->
    cartridge
      |> Cartridge.readRom (int address)
  )


  let private read4000 cartridge mbc5 = Reader (fun address ->
    cartridge
      |> Cartridge.readRom (((int address) &&& 0x3fff) ||| mbc5.romPage)
  )


  let private readA000 cartridge mbc5 = Reader (fun address ->
    if not mbc5.ramEnabled then
      0x00uy
    else
      Cartridge.readRam (ramAddress address mbc5) cartridge
  )


  let private write0000 mbc5 = Writer (fun _ data ->
    mbc5.ramEnabled <- data = 0x0auy
  )


  let private write2000 mbc5 = Writer (fun _ data ->
    mbc5.romPage <- (int data) <<< 14)


  let private write3000 mbc5 = Writer (fun _ data ->
    mbc5.romPage <- mbc5.romPage ||| (((int data) &&& 0x01) <<< 22)
  )


  let private write4000 mbc5 = Writer (fun _ data ->
    mbc5.ramPage <- ((int data) &&& 0x0f) <<< 13
  )


  let private writeA000 cartridge mbc5 = Writer (fun address data ->
    if mbc5.ramEnabled then
      cartridge
        |> Cartridge.writeRam (ramAddress address mbc5) data
  )


  let ramSize value =
    match value with
    | 0uy -> (null, 0)
    | 1uy -> (Array.zeroCreate<byte> 0x02000, 0x01fff)
    | 2uy -> (Array.zeroCreate<byte> 0x08000, 0x07fff)
    | 3uy -> (Array.zeroCreate<byte> 0x20000, 0x1ffff)
    | _ ->
      failwith ""


  let read cartridge mbc5 = Reader (fun address ->
    match int address with
    | n when n >= 0x0000 && n <= 0x3fff -> Reader.run (read0000 cartridge mbc5) address
    | n when n >= 0x4000 && n <= 0x7fff -> Reader.run (read4000 cartridge mbc5) address
    | n when n >= 0xa000 && n <= 0xbfff -> Reader.run (readA000 cartridge mbc5) address
    | _ ->
      0xffuy
  )


  let write cartridge mbc5 = Writer (fun address data ->
    match int address with
    | n when n >= 0x0000 && n <= 0x1fff -> Writer.run (write0000 mbc5) address data
    | n when n >= 0x2000 && n <= 0x2fff -> Writer.run (write2000 mbc5) address data
    | n when n >= 0x3000 && n <= 0x3fff -> Writer.run (write3000 mbc5) address data
    | n when n >= 0x4000 && n <= 0x5fff -> Writer.run (write4000 mbc5) address data
    | n when n >= 0x6000 && n <= 0x7fff -> ()
    | n when n >= 0xa000 && n <= 0xbfff -> Writer.run (writeA000 cartridge mbc5) address data
    | _ ->
      ()
  )
