module GameBoy.APU.Sweep

let overflowCheck (state: Sq1State) =
  let period =
    if state.sweepDirection = 0 then
      state.period + (state.period >>> state.sweepShift)
    else
      state.period - (state.period >>> state.sweepShift)

  if period > 0x7ff then
    state.enabled <- false
  else
    state.period <- max period 0


let tick (state: Sq1State) =
  if state.sweepTimer <> 0 then
    state.sweepTimer <- state.sweepTimer - 1

    if state.sweepTimer = 0 && state.sweepEnabled && state.sweepPeriod <> 0 then
      state.sweepTimer <- state.sweepPeriod
      overflowCheck(state)
