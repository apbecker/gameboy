namespace GameBoy.APU

type Duration =
  { mutable enabled: bool
    mutable counter: int }

module Duration =

  let init () =
    { enabled = false
      counter = 0 }


  let tick duration =
    if duration.enabled && duration.counter <> 0 then
      duration.counter <- duration.counter - 1

    duration.counter = 0
