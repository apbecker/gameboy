namespace GameBoy.APU

type WavState =
  { mutable enabled: bool
    mutable period: int
    mutable timer: int

    duration: Duration

    regs: byte []

    mutable dacPower: bool
    mutable volumeCode: int
    mutable volumeShift: int

    mutable waveRamSample: byte
    mutable waveRamCursor: int
    mutable waveRamOutput: int
    mutable waveRamShift: int }

module WavState =

  open GameBoy


  let init () =
    { enabled = false
      period = 0
      timer = 0

      duration = Duration.init ()

      regs = Array.zeroCreate 5

      dacPower = false
      volumeCode = 0
      volumeShift = 4

      waveRamSample = 0uy
      waveRamCursor = 0
      waveRamOutput = 0
      waveRamShift = 0 }


  let read wav = Reader (fun address ->
    match address with
    | 0xff1aus -> 0x7fuy ||| wav.regs.[0]
    | 0xff1bus -> 0xffuy ||| wav.regs.[1]
    | 0xff1cus -> 0x9fuy ||| wav.regs.[2]
    | 0xff1dus -> 0xffuy ||| wav.regs.[3]
    | 0xff1eus -> 0xbfuy ||| wav.regs.[4]
    | _ ->
      failwith ""
  )


  let write wav = Writer (fun address data ->
    let index = (int address) - 0xff1a
    wav.regs.[index] <- byte data

    match address with
    | 0xff1aus ->
      wav.dacPower <- ((int data) &&& 0x80) <> 0
      if not wav.dacPower then
        wav.enabled <- false

    | 0xff1bus ->
      wav.duration.counter <- 256 - (int data)

    | 0xff1cus ->
      wav.volumeCode <- ((int data) >>> 5) &&& 3

      match wav.volumeCode with
      | 0 -> wav.volumeShift <- 4
      | 1 -> wav.volumeShift <- 0
      | 2 -> wav.volumeShift <- 1
      | 3 -> wav.volumeShift <- 2
      | _ -> failwith ""

    | 0xff1dus ->
      wav.period <- (wav.period &&& 0x700) ||| (((int data) <<< 0) &&& 0x0ff)

    | 0xff1eus ->
      wav.period <- (wav.period &&& 0x0ff) ||| (((int data) <<< 8) &&& 0x700)
      wav.duration.enabled <- ((int data) &&& 0x40) <> 0

      if ((int data) &&& 0x80) <> 0 && wav.dacPower then
        wav.timer <- (0x800 - wav.period) * 2
        wav.waveRamCursor <- 0
        wav.waveRamShift <- 4
        wav.enabled <- true

        if wav.duration.counter = 0 then
          wav.duration.counter <- 256

    | _ ->
      failwith ""
  )


  let sample wav =
    if wav.enabled then
      wav.waveRamOutput >>> wav.volumeShift
    else
      0
