namespace GameBoy

[<Struct>]
type Reader =
  | Reader of (uint16 -> uint8)

module Reader =

  let run (Reader f) address =
    f address


[<Struct>]
type Writer =
  | Writer of (uint16 -> uint8 -> unit)

module Writer =

  let run (Writer f) address data =
    f address data
