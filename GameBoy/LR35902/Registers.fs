namespace GameBoy.Processors.LR35902

type Registers =
  { mutable b: byte
    mutable c: byte
    mutable d: byte
    mutable e: byte
    mutable h: byte
    mutable l: byte
    mutable a: byte
    mutable f: byte
    mutable sp: uint16
    mutable pc: uint16
    mutable aa: uint16 }

module Registers =

  let init () =
    { b = 0uy
      c = 0uy
      d = 0uy
      e = 0uy
      h = 0uy
      l = 0uy
      a = 0uy
      f = 0uy
      sp = 0us
      pc = 0us
      aa = 0us }


  let makeWord upper lower =
    let upper = uint16 upper
    let lower = uint16 lower
    ((upper <<< 8) ||| lower)


  let bc regs = makeWord regs.b regs.c


  let de regs = makeWord regs.d regs.e


  let hl regs = makeWord regs.h regs.l


  let af regs = makeWord regs.a regs.f


  let pc regs = regs.pc


  let pch regs = byte (regs.pc >>> 8)


  let pcl regs = byte (regs.pc >>> 0)


  let sp regs = regs.sp


  let sph regs = byte (regs.sp >>> 8)


  let spl regs = byte (regs.sp >>> 0)


  let ea regs = regs.aa


  let withBC (value: uint16) regs =
    regs.b <- byte (value >>> 8)
    regs.c <- byte (value >>> 0)


  let withDE (value: uint16) regs =
    regs.d <- byte (value >>> 8)
    regs.e <- byte (value >>> 0)


  let withHL (value: uint16) regs =
    regs.h <- byte (value >>> 8)
    regs.l <- byte (value >>> 0)


  let withAF (value: uint16) regs =
    regs.a <- byte (value >>> 8)
    regs.f <- byte (value >>> 0)


  let withSP value regs =
    regs.sp <- value


  let withPC value regs =
    regs.pc <- value


  let withEA value regs =
    regs.aa <- value


  let mapBC f regs =
    withBC (f (bc regs)) regs


  let mapDE f regs =
    withDE (f (de regs)) regs


  let mapHL f regs =
    withHL (f (hl regs)) regs


  let mapSP f regs =
    regs.sp <- f regs.sp


  let mapPC f regs =
    regs.pc <- f regs.pc
