namespace GameBoy.Processors.LR35902

type Status =
  { mutable z: int
    mutable n: int
    mutable h: int
    mutable c: int }

module Status =

  let init () =
    { z = 0
      n = 0
      h = 0
      c = 0 }


  let load (value: byte) status =
    status.z <- ((int value) >>> 7) &&& 1
    status.n <- ((int value) >>> 6) &&& 1
    status.h <- ((int value) >>> 5) &&& 1
    status.c <- ((int value) >>> 4) &&& 1


  let save status =
    let packed =
      (status.z <<< 7) |||
      (status.n <<< 6) |||
      (status.h <<< 5) |||
      (status.c <<< 4)

    byte packed
