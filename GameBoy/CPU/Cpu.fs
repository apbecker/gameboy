namespace GameBoy.CPU

open GameBoy.Processors.LR35902

type Interrupt =
  | VBlank = 1
  | Status = 2
  | Elapse = 4
  | Serial = 8
  | Joypad = 16

type CpuState =
  { core: Core
    mutable ief: byte
    mutable irf: byte }

module Cpu =

  let init () =
    { core = Core.init ()
      ief = 0uy
      irf = 0uy }


  let (|RST|_|) mask flags =
    if (mask &&& flags) <> 0uy then
      Some()
    else
      None


  let vector writer flags cpu =
    let address =
      match flags with
      | RST(0x01uy) ->
        cpu.irf <- cpu.irf ^^^ 0x01uy
        0x40us

      | RST(0x02uy) ->
        cpu.irf <- cpu.irf ^^^ 0x02uy
        0x48us

      | RST(0x04uy) ->
        cpu.irf <- cpu.irf ^^^ 0x04uy
        0x50us

      | RST(0x08uy) ->
        cpu.irf <- cpu.irf ^^^ 0x08uy
        0x58us

      | RST(0x10uy) ->
        cpu.irf <- cpu.irf ^^^ 0x10uy
        0x60us

      | _ ->
        failwith ""

    cpu.core |> Core.rst writer address
    12


  let update reader writer cpu =
    let mutable cyclesForThisInstruction = cpu.core |> Core.step reader writer

    if cpu.core.ime then
      let flags = cpu.irf &&& cpu.ief
      if flags <> 0uy then
        cpu.core.ime <- false
        cyclesForThisInstruction <- cyclesForThisInstruction + (cpu |> vector writer flags)

    cyclesForThisInstruction


  let irq interrupt cpu =
    let flag = byte interrupt
    cpu.irf <- cpu.irf ||| flag

    if (cpu.ief &&& flag) <> 0uy then
      cpu.core.halt <- false

      if interrupt = Interrupt.Joypad then
        cpu.core.stop <- false
