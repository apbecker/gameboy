namespace GameBoy.Platform

[<Struct>]
type Memory =
  | Ram of Ram:(byte array * int)
  | Rom of Rom:(byte array * int)

module Memory =

  open GameBoy.Platform.Util


  let rom buffer =
    Rom (buffer, buffer.Length - 1)


  let private nextPowerOfTwo number =
    let copyBits x n =
      n ||| (n >>> x)

    let addOne n = n + 1
    let subOne n = n - 1

    number
      |> subOne
      |> copyBits 1
      |> copyBits 2
      |> copyBits 4
      |> copyBits 8
      |> copyBits 16
      |> addOne


  let ram capacity =
    let buffer =
      BinaryUnit.byteSize capacity
        |> nextPowerOfTwo
        |> Array.zeroCreate

    Ram (buffer, buffer.Length - 1)


  let get address memory =
    match memory with
    | Ram (buffer, mask) -> buffer.[address &&& mask]
    | Rom (buffer, mask) -> buffer.[address &&& mask]


  let put address data memory =
    match memory with
    | Ram (buffer, mask) -> buffer.[address &&& mask] <- data
    | Rom _ ->
      ()
