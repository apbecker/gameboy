namespace GameBoy.Platform.Video

type VideoSink =
  | VideoSink of (int -> int -> int -> unit)

module VideoSink =

  let run (VideoSink f) x y color =
    f x y color
