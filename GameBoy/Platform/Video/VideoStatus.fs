namespace GameBoy.Platform.Video

[<RequireQualifiedAccess>]
type VideoStatus =
  | Continue
  | Exit
