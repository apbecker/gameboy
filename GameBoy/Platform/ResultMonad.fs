namespace GameBoy.Platform.Util

[<RequireQualifiedAccess>]
module Result =

  let get =
    function
    | Ok(value) -> value
    | Error(error) -> failwithf "Error: %A" error


[<AutoOpen>]
module ResultMonad =

  type ResultBuilder () =
    member __.Bind(ma, f) = Result.bind f ma
    member __.Return a = Ok a
    member __.ReturnFrom a = a


  let result = ResultBuilder ()
