namespace GameBoy.Platform.Audio

[<RequireQualifiedAccess>]
type AudioBackend =
  | Null
  | SDL2 of SDL2Audio

module AudioBackend =

  let create (definition: AudioDefinition) =
    match definition.backend with
    | "sdl2" ->
      AudioBackend.SDL2 <| SDL2Audio.create definition

    | _ ->
      AudioBackend.Null


  let destroy audio =
    match audio with
    | AudioBackend.Null ->
      ()

    | AudioBackend.SDL2(sdl2) ->
      SDL2Audio.destroy sdl2


  let getSink audio =
    match audio with
    | AudioBackend.Null ->
      AudioSink(ignore)

    | AudioBackend.SDL2(sdl2) ->
      SDL2Audio.sink(sdl2)


  let render audio =
    match audio with
    | AudioBackend.Null ->
      ()

    | AudioBackend.SDL2(sdl2) ->
      SDL2Audio.render sdl2
