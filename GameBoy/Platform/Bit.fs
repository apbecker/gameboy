namespace GameBoy.Platform.Util

type Bit =
  | Bit of int

[<RequireQualifiedAccess>]
module Bit =

  let getMask n (Bit bit) =
    BitValue.toInt32 n <<< bit


  let get n bit =
    n &&& (getMask On bit)
      |> BitValue.ofInt32


  let set bit value n =
    n ||| (getMask value bit)


  let clear bit n =
    match get n bit with
    | Off -> n
    | On  -> n ^^^ (getMask On bit)


  let swap bit1 bit2 value =
    let value1 = get value bit1 in
    let value2 = get value bit2 in

    value
      |> clear bit1
      |> clear bit2
      |> set bit1 value2
      |> set bit2 value1
