[<RequireQualifiedAccess>]
module GameBoy.Platform.Util.Byte

let private reverseBits value =
  value
    |> Bit.swap (Bit 0) (Bit 7)
    |> Bit.swap (Bit 1) (Bit 6)
    |> Bit.swap (Bit 2) (Bit 5)
    |> Bit.swap (Bit 3) (Bit 4)


let private reverseTable =
  Seq.init 256 reverseBits
    |> Seq.map uint8
    |> Seq.toArray


let reverse n =
  reverseTable.[n]
